import React from 'react';
// import React, { Component } from 'react';

const Header = (props) => {
 const dishes = props.food.map(dish => <ul><li>{dish}</li></ul>)
 return ( 
  <>
  <h1>Moje ulubione dania:</h1>
  {dishes}
  </>
  );
}

// class Header extends Component { 
 
//  render() { 
//   const dishes = this.props.food.map(dish => <ul><li>{dish}</li></ul>)

//   return ( 
//    <>
//     {dishes}
//    </>
//    );
//  } 
// }
 
export default Header;
 
